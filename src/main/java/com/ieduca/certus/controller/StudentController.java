package com.ieduca.certus.controller;

import com.ieduca.certus.dto.CustomResponseDto;
import com.ieduca.certus.dto.UpdatePeriodRequestDto;
import com.ieduca.certus.model.Student;
import com.ieduca.certus.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("student")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("{id}")
    public Student getStudent(@PathVariable Long id) {
        return studentService.getStudentById(id);
    }

    @PostMapping("update-period")
    public ResponseEntity<CustomResponseDto> updatePeriodOfStudent(@RequestBody UpdatePeriodRequestDto updatePeriodRequestDto) {
        if (studentService.upgradePeriodOfStudent(updatePeriodRequestDto.getId(), updatePeriodRequestDto.getPeriod())) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(new CustomResponseDto(HttpStatus.OK.value(), "Semester was updated!"));
        }

        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new CustomResponseDto(HttpStatus.BAD_REQUEST.value(), "Semester could not be updated!"));
    }
}

package com.ieduca.certus.service;

import com.ieduca.certus.model.Student;
import com.ieduca.certus.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Student getStudentById(Long id) {
        return studentRepository.findById(id).orElse(new Student());
    }

    public boolean upgradePeriodOfStudent(Long id, int period) {
        Student student = studentRepository.findById(id).orElse(new Student());

        if (period < student.getSemester()) {
            return false;
        }

        student.setSemester(period);
        studentRepository.save(student);

        return true;
    }
}

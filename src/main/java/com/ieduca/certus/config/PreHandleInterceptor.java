package com.ieduca.certus.config;

import com.ieduca.certus.util.AuthorizationHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class PreHandleInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!request.getServletPath().equals("/error")) {
            String studentId = request.getServletPath().replace("/student/", "").trim();

            int studentIdInt = Integer.parseInt(studentId);

            if (!AuthorizationHandler.isAuthorized(studentIdInt)) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error for auth");
                return false;
            }
        }

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}

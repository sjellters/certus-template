package com.ieduca.certus.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class WebMvcAppConfig implements WebMvcConfigurer {

    private final PreHandleInterceptor preHandleInterceptor;

    @Autowired
    public WebMvcAppConfig(PreHandleInterceptor preHandleInterceptor) {
        this.preHandleInterceptor = preHandleInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(preHandleInterceptor);
    }
}
